<?php

declare(strict_types=1);

namespace Drupal\s3client;

use Aws\Credentials\Credentials;
use Aws\S3\S3Client;
use Aws\S3\S3ClientInterface;
use Drupal\Core\Site\Settings;

/**
 * Defines an s3 client factory.
 */
class S3ClientFactory implements S3ClientFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function getDefaultClient(): S3ClientInterface {
    $credentials = new Credentials(
      Settings::get('s3client.default_key'),
      Settings::get('s3client.default_secret'),
    );
    return new S3Client([
      'credentials' => $credentials,
      'version' => '2006-03-01',
      'region' => Settings::get('s3client.default_region'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function createClient(string $key, string $secret, string $region): S3ClientInterface {
    $credentials = new Credentials(
      $key,
      $secret,
    );
    return new S3Client([
      'credentials' => $credentials,
      'version' => '2006-03-01',
      'region' => $region,
    ]);
  }

}
