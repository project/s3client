<?php

declare(strict_types=1);

namespace Drupal\s3client;

use Aws\S3\S3ClientInterface;

/**
 * Defines an s3 client factory.
 */
interface S3ClientFactoryInterface {

  /**
   * Gets S3 client.
   *
   * @return \Aws\S3\S3Client
   *   Client.
   */
  public function getDefaultClient(): S3ClientInterface;

  /**
   * Creates an S3 client.
   *
   * @param string $key
   *   Key.
   * @param string $secret
   *   Secret.
   * @param string $region
   *   Region.
   *
   * @return \Aws\S3\S3Client
   *   Client.
   */
  public function createClient(string $key, string $secret, string $region): S3ClientInterface;

}
